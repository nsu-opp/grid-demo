#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>

typedef struct Grid2D Grid2D;
struct Grid2D {
    float *Data;
    int N;
    float X0;
    float Y0;
    float D;

    int LowerEdge;
    int UpperEdge;
};

typedef float (*Function)(float x, float y);

typedef float (*GridFunction)(Grid2D grid, int i, int j);

Grid2D Grid2D_New(int size, float x0, float y0, float realSize) {
    assert(size >= 1 && "Size must be >= 1");

    float *const data = (float *) calloc(size * size, sizeof(*data));

    return (Grid2D) {
            .Data = data,
            .N = size,
            .X0 = x0,
            .Y0 = y0,
            .D = realSize,
            .LowerEdge = 0,
            .UpperEdge = size - 1,
    };
}

Grid2D Grid2D_Copy(Grid2D grid) {
    float *const data = (float *) calloc(grid.N * grid.N, sizeof(*data));
    memcpy(data, grid.Data, sizeof(*data) * grid.N * grid.N);

    Grid2D copy = grid;
    copy.Data = data;

    return copy;
}

void Grid2D_Free(Grid2D grid) {
    free(grid.Data);
}

float *Grid2D_At(Grid2D grid, int i, int j) {
    return &grid.Data[j * grid.N + i];
}

float Grid2D_IndexToX(
        Grid2D grid,
        int i) {
    return grid.X0 + grid.D * (float) i / (float) (grid.N - 1);
}

float Grid2D_IndexToY(
        Grid2D grid,
        int j) {
    return grid.Y0 + grid.D * (float) j / (float) (grid.N - 1);
}

void Grid2D_Print(FILE *file, Grid2D grid) {
    for (int j = 0; j < grid.N; j += 1) {
        for (int i = 0; i < grid.N; i += 1) {
            fprintf(file, "%.2f ", *Grid2D_At(grid, i, j));
        }

        fprintf(file, "\n");
    }
}

float Grid2D_MaxDiff(Grid2D grid, Function target) {
    float delta = 0.0f;

    for (int j = 0; j < grid.N; j += 1) {
        for (int i = 0; i < grid.N; i += 1) {
            const float x = Grid2D_IndexToX(grid, i);
            const float y = Grid2D_IndexToY(grid, j);
            const float currentDelta = fabsf(target(x, y) - *Grid2D_At(grid, i, j));

            delta = fmaxf(delta, currentDelta);
        }
    }

    return delta;
}


void Grid2D_Swap(Grid2D *g1, Grid2D *g2) {
    Grid2D tmp = *g1;
    *g1 = *g2;
    *g2 = tmp;
}


float Phi(float x, float y) {
    return x * x + y * y;
}


void FillEdges(
        Grid2D grid,
        Function target_fn) {
    float x, y;
    for (int i = 0; i < grid.N; i += 1) {
        x = Grid2D_IndexToX(grid, i);

        *Grid2D_At(grid, i, grid.LowerEdge) = target_fn(x, Grid2D_IndexToY(grid, grid.LowerEdge));
        *Grid2D_At(grid, i, grid.UpperEdge) = target_fn(x, Grid2D_IndexToY(grid, grid.UpperEdge));

        y = Grid2D_IndexToY(grid, i);

        *Grid2D_At(grid, grid.LowerEdge, i) = target_fn(Grid2D_IndexToX(grid, grid.LowerEdge), y);
        *Grid2D_At(grid, grid.UpperEdge, i) = target_fn(Grid2D_IndexToX(grid, grid.UpperEdge), y);
    }
}


void Approximate(Grid2D grid, GridFunction step, float targetDelta) {
    float iter_delta = INFINITY;
    int n_iter = 0;

    Grid2D next = Grid2D_Copy(grid);

    while (iter_delta > targetDelta && n_iter < 20) {
#ifdef PRINT_ITER
        printf("n_iter = %d\niter_delta = %.8f\n", n_iter, iter_delta);
        Grid2D_Print(stdout, grid);
        printf("\n");
#endif // PRINT_ITER
        iter_delta = 0;

        for (int j = grid.LowerEdge + 1; j <= grid.UpperEdge - 1; j += 1) {
            for (int i = grid.LowerEdge + 1; i <= grid.UpperEdge - 1; i += 1) {
                const float old_value = *Grid2D_At(grid, i, j);
                const float new_value = step(grid, i, j);
                // printf("[%d, %d] %.2f -> %.2f\n",i, j, old_value, new_value);
                *Grid2D_At(next, i, j) = new_value;

                const float current_delta = fabsf(new_value - old_value);
                iter_delta = fmaxf(iter_delta, current_delta);
            }
        }

        Grid2D_Swap(&next, &grid);

        n_iter += 1;
        printf("n_iter = %d\n", n_iter);
    }

    if (n_iter % 2 != 0) {
        Grid2D_Swap(&grid, &next);
    }
    Grid2D_Free(next);

#ifdef PRINT_ITER
    printf("DONE\nn_iter = %d\niter_delta = %.8f\n", n_iter, iter_delta);
    Grid2D_Print(stdout, grid);
    printf("\n");
#endif // PRINT_ITER
}


float ApproximateValue(Grid2D grid, int i, int j) {
    const float x = Grid2D_IndexToX(grid, i);
    const float y = Grid2D_IndexToY(grid, j);
    const float a = 1e5f;

    const float h = grid.D / (float) (grid.N - 1);
    const float hxSquared = h * h;
    const float hySquared = h * h;

    const float C = 1.0f / (a + 2.0f / hxSquared + 2.0f / hySquared);
    const float xPart = (*Grid2D_At(grid, i - 1, j) + *Grid2D_At(grid, i + 1, j)) / hxSquared;
    const float yPart = (*Grid2D_At(grid, i, j - 1) + *Grid2D_At(grid, i, j + 1)) / hySquared;
    const float rho = 6 - a * Phi(x, y);

    return C * (xPart + yPart - rho);
}


int main(void) {
    const float eps = 1e-8f;

    const int grid_size = 31;
    const float grid_min = -1.0f;
    const float grid_real_size = 2.0f;

    Grid2D grid = Grid2D_New(grid_size, grid_min, grid_min, grid_real_size);

    Function target_fn = Phi;
    FillEdges(grid, target_fn);
    Approximate(grid, ApproximateValue, eps);

    printf("max_delta = %.8f\n", Grid2D_MaxDiff(grid, target_fn));

    Grid2D_Free(grid);

    return EXIT_SUCCESS;
}