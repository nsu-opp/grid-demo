set -x

mkdir -p bin
gcc -Wall -Werror -Wextra -O3 $@ -o bin/main main.c
